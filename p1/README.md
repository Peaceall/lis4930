> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4930 - Extensible Enterprise Solutions

## Juan Abreut

### Project 1 Requirements:

*Sub-Heading:*

1. SplashScreen
2. Mp3 implemented
3. Invisible buttons on play

> This is my blockquote.
> 
> This is the second paragraph in the blockquote as an example.
>
> #### Git commands w/short descriptions:

1. git init - Initializes repo
2. git status - Checks current status of repo
3. git add - Adds current changes to que for a push
4. git commit - Commits change of local repo
5. git push - Pushes changes to the server
6. git pull - pulls changes from online repo
7. git branch -lists all branches

#### Assignment Screenshots:

Screenshot of splash

![Ampps Installation Screenshot](img/a3_1.png)

Screenshot of all buttons

![HelloWorld Running Screenshot](img/a3_2.png)

Screenshot of invisible buttons

![HelloWorld Running Screenshot](img/a3_3.png)

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/Peaceall/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/Peaceall/myteamquotes/ "My Team Quotes Tutorial")
