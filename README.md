# LIS4930 - Advanced Mobile Application Development

## Juan Abreut

### Assignments: 
 
* [In this repo: A1 README.md](a1)

    * Install AMPPS

    * Install JDK

    * Install Android Studio and Create My First App

    * Create and run a Contacts App

    * Create Bitbucket repo

    * Complete Bitbucket tutorials (see bottom)

    * Provide Git command descriptions

* [In this repo: A2 README.md](a2)

    * Create and run a Tip App

    * Show with and without fields
	
	* Implement dropdown menus.
	
* [In this repo: P1 README.md](p1)

	* Creation of Currency Converter application
	
	* Implement toast notification
	
	* Include launcher icon
	
* [In this repo: A3 README.md](a3) 

	* SplashScreen implementation
	
	* Mp3 playback
	
	* Dynamic button interaction
	
* [In this repo: A4 README.md](a4)

	* Mortgage calculation
	
	* persistent data
	
	* Launcher icon
	
* [In this repo: P2 README.md](p2)

	* TaskList Application
	
	* SQLite Implementation
	
	* Launcher icon included
	
* [BitBucketStationLocations Tutorial](https://bitbucket.org/Peaceall/bitbucketstationlocations)
 
* [Myteamquotes Tutorial](https://bitbucket.org/Peaceall/myteamquotes)