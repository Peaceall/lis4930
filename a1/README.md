> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369 - Extensible Enterprise Solutions

## Juan Abreut

### Assignment 1 Requirements:

*Sub-Heading:*

1. To show version Control Mastery
2. Show Developmental Installation Success
3. Chapter questions

#### README.md file should include the following items:`

* Screenshot of hwapp application running
* Screenshot of aspnetcoreapp running 
* git commands with short descriptions
* Bitbucket tutorials

> This is my blockquote.
> 
> This is the second paragraph in the blockquote as an example.
>
> #### Git commands w/short descriptions:

1. git init - Initializes repo
2. git status - Checks current status of repo
3. git add - Adds current changes to que for a push
4. git commit - Commits change of local repo
5. git push - Pushes changes to the server
6. git pull - pulls changes from online repo
7. git branch -lists all branches

#### Assignment Screenshots:

Screenshot of ampps 

![Ampps Installation Screenshot](img/a1_1.png)

Screenshot of hello world application running:

![HelloWorld Running Screenshot](img/a1_2.png)

Screenshot of Hello World running as an android application:

![HelloWorld Android Screenshot](img/a1_3.png)

Screenshot of Contact Apps main:

![HelloWorld Running Screenshot](img/a1_4.png)

Screenshot of hello world application running:

![HelloWorld Running Screenshot](img/a1_5.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/Peaceall/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/Peaceall/myteamquotes/ "My Team Quotes Tutorial")
